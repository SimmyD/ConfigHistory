from django.db import models

# Create your models here.
class device(models.Model):
	name = models.CharField(max_length=30)
	ip_address = models.IPAddressField()
	model = models.ForeignKey('model')	

	def __unicode__(self):
		return self.name

class model(models.Model):
	name = models.CharField(max_length=30)
	
        def __unicode__(self):
        	return self.name

